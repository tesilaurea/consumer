package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/streadway/amqp"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

/* Qui inserisco la classe di qui devo fare il Json parser
 */

func main() {
	pathRabbitMQ := "amqp://guest:guest@" + os.Getenv("RABBITMQ_HOST") + ":5672/"
	//"amqp://guest:guest@localhost:5672/" //arg[0]
	pathMongoGet := "http://localhost:8081/workflow/get/"
	pathToAssemblator := "http://localhost:8082/postAssemblateWorkflow/"
	pathMongoPut := "http://localhost:8081/workflow/deployed/"
	queue := "deploy"

	conn, err := amqp.Dial(pathRabbitMQ)
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(
		queue, // name
		false, // durable
		false, // delete when unused
		false, // exclusive
		false, // no-wait
		nil,   // arguments
	)
	failOnError(err, "Failed to declare a queue")

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	failOnError(err, "Failed to register a consumer")

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			var deployTask DeployTask
			var workflowAbstract WorkflowAbstract

			log.Printf("Received a message: %s", d.Body)
			//prendere da MongoDB il metadato del workflow
			Json2Struct(&deployTask, d.Body)

			//---------------------------------------------
			resp, err := http.Get(pathMongoGet + deployTask.Uid)
			if err != nil {
				// handle error
				panic(err)
			}
			body, err := ioutil.ReadAll(resp.Body)

			if err := json.Unmarshal(body, &workflowAbstract); err != nil {
				panic(err)
			}
			fmt.Println(workflowAbstract)
			//---------------------------------------------

			response, err := http.Post(pathToAssemblator+deployTask.Uid, "application/json; charset=UTF-8", bytes.NewBuffer(body))
			if err != nil {
				// handle error
				//panic(err)
				fmt.Println("Attenzione! non si è riusciti a ottenere un mapping! per " + workflowAbstract.Uid)
			}

			// se resp.status != Ok => allora manda un feedback al client (bisogna solo rifare il deploy
			// questo fallimento non ha grandi conseguenze)
			if response.StatusCode == http.StatusOK {
				// deploy è 1
				_, err := http.Get(pathMongoPut + deployTask.Uid)
				if err != nil {
					// handle error
					//panic(err)
					fmt.Println("Attenzione! non si è riusciti ad aggiornare il campo isdeploy per " + workflowAbstract.Uid)
				}
			} else {
				fmt.Println("Attenzione! non si è riusciti a ottenere un mapping per " + workflowAbstract.Uid)
			}
		}

	}()

	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
}
